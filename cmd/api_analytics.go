package main

import (
	"gitlab.com/K-96/api-analytics/internal/handles"
	"gitlab.com/K-96/api-analytics/internal/middleware"

	"flag"
	"log"
	"net/http"
)

const defaultHttpPort = "8000"

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/changePost", handles.ChangePost)

	handler := middleware.LogTime(middleware.Dilatory(mux))

	err := http.ListenAndServe(":"+resolveHttpPort(), handler)
	if err != nil {
		log.Fatalf("error starting server: %v", err)
	}
}

func resolveHttpPort() (port string) {
	flag.StringVar(&port, "http-port", defaultHttpPort, "port that HTTP server will listen on")
	flag.Parse()

	if port == "" {
		port = defaultHttpPort
	}
	return
}
