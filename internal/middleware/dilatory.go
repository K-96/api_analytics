package middleware

import (
	"math/rand"
	"net/http"
	"time"
)

func Dilatory(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rand.Seed(time.Now().UnixNano())
		sec := rand.Intn(15-3) + 3
		time.Sleep(time.Duration(sec) * time.Second)
		next.ServeHTTP(w, r)
	})
}
