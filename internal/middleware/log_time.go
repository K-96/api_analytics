package middleware

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

func LogTime(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		s := time.Now()
		next.ServeHTTP(w, req)

		b, err := json.Marshal(struct {
			Method     string        `json:"method"`
			RequestURI string        `json:"requestUri"`
			Duration   time.Duration `json:"duration"`
		}{
			Method:     req.Method,
			RequestURI: req.RequestURI,
			Duration:   time.Since(s) / time.Second,
		})
		if err != nil {
			log.Print(err)
		}

		log.Print(string(b))
	})
}
