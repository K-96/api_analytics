package handles

import (
	"encoding/json"
	"net/http"
)

type Post struct {
	AuthorId  int64  `json:"authorId"`
	PostId    int64  `json:"postId"`
	PostTitle string `json:"postTitle"`
	Action    string `json:"action"`
}

func ChangePost(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "only POST requests are allowed", http.StatusNotFound)
		return
	}

	post := Post{}

	json.NewDecoder(r.Body).Decode(&post)

	if post.PostId%3 == 0 {
		http.Error(w, "post id is a multiple of 3", http.StatusInternalServerError)
	}
}
